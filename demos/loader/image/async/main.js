
$(function(){
    var loader = new SNR.loader.image.Async(false),
        imgs = [];

    $.each($("img"), function(val, index) {
        imgs.push({ img: $(this)[0], src: $(this).data("src") });
    });

    loader.init();

    //console.log("imgs: ", imgs[0]);
    loader.addListener(loader.LOAD_SUCCESS, handleLoadSuccess);
    loader.addListener(loader.LOAD_COMPLETE, handleLoadComplete);
    loader.load(imgs);

    function handleLoadSuccess(evt) {
       // console.log("load success: ", evt.img);
    }

    function handleLoadComplete() {
       // console.log("load complete");
    }
});