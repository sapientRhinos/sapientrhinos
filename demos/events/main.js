/**
 * @author Matt Przybylski
 */
var main = (function($, document, window, undefined) {
    var dispatcher = new SNR.events.EventDispatcher(),
        i = 0,
        isOddClick = true,

        // selectors
        $box1 = $('#box1'),
        $box2 = $('#box2'),
        $box3 = $('#box3'),
        $btn = $('#doStuff');

    /* Public Methods _________________________________________________________________ */

    /**
     *
     */
    function init() {
        $btn.on('click', handleDoStuff);

        dispatcher.add('change.colors', handleColorsChanged);
        dispatcher.add('change.colors', handleColorsLog);
    }

    /* Private Methods _________________________________________________________________ */

    /**
     *
     */
    function handleDoStuff(evt) {
        var colors = null;

        if (isOddClick) {
            colors = {color1: '#000000', color2: '#ffff00', color3: '#333333'};
        } else {
            colors = {color1: '#ff0000', color2: '#00ff00', color3: '#0000ff'};
        }

        dispatcher.dispatch('change.colors', colors);
        isOddClick = !isOddClick;
        i++;

        if (i >= 4 && i < 8) {
            dispatcher.remove('change.colors', handleColorsChanged);

            console.log('color changing listener has been removed, no more color changing from here on out');
        } else if (i === 8) {
            dispatcher.removeAll();

            console.log('removing all listeners');
        }
    }

    /**
     *
     */
    function handleColorsChanged(colors) {
        $box1.css({'background-color': colors.color1});
        $box2.css({'background-color': colors.color2});
        $box3.css({'background-color': colors.color3});
    }

    /**
     *
     */
    function handleColorsLog() {
        console.log('this will keep running until all listeners are removed...');
    }

    /**
     * Expose API publicly
     */
    return {
        init: init
    };
}(jQuery, document, window, undefined));

/**
 *
 */
jQuery(document).ready(function() {
    main.init();
});