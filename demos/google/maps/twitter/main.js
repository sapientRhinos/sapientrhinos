$(function() {
	// add the map
	var map = new google.maps.Map($('#map')[0], {
		zoom: 4,
		// center of US for now
		center: new google.maps.LatLng(37.6970,-91.8096),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}),
	// initialize the views here, renderer,
	renderer = new google.maps.DirectionsRenderer(),
	googlemaps = SNR.google.maps.utils;

	// initialize our google maps util
	googlemaps.init({ map: map, directionsRenderer : renderer });

	// call SNR.google.maps.twitter
	SNR.google.maps.twitter.search(map, '#livingthelife');

});