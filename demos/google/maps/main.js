

$(function() {
	var toronto = new google.maps.LatLng(43.652527,-79.381961),
		renderer = new google.maps.DirectionsRenderer(),  // initialize the views here, renderer
		map = new google.maps.Map($('#map_canvas')[0], { // and the map
				zoom: 14,
				center: toronto,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}),
		cl = null,
		MAX_DISTANCE = 40, // 40km
		MAX_MARKERS = 2, // the maximum number of markers on the map, Google docs states 9 max
		ratio = 90 / MAX_DISTANCE, // the ratio of degrees to distance for the gauge rotation
		oldRotation = 45, // the starting rotation for the gauge
		markers = [], // an array of markers(THIS IS NEEDED for some methods of SNR.google.maps)
		waypoints = [], // an array of points on the map
		origin = null, // origin latLng
		destination = null, // destination latLng
		googlemaps = SNR.google.maps.utils; // simple reference to save typing

	googlemaps.init({ map: map, directionsRenderer : renderer }); // initialize our google maps util
	google.maps.event.addListener(map, 'click', handleMapClick); // listen for map clicks


	// event handlers that are dispatched from SNR.google.maps
	$(document).bind(SNR.google.maps.utils.DISTANCE_CHANGED, handleDirectionChange);
	$(document).bind(SNR.google.maps.utils.GEOLOCATION_SUCCESS, handleGeolocation);
	$(document).bind(SNR.google.maps.utils.ROUTE_SUCCESS, handleRouteSuccess);

	// ______________ ELEVATION TEST _________________
	$(document).bind(SNR.google.maps.utils.ELEVATION_SUCCESS, function(evt, elev){
		console.log("elevation: ",elev); // Test the elevation
	});
	googlemaps.getElevation(toronto);

	// ______________ GEOLOCATION TEST _________________
	function handleGeolocation(evt, latLng) {
		console.log("location: ", latLng);
	}
	googlemaps.geolocate();

	// ______________ STATIC MAP TEST _________________
	$("#getMap").on("click", function(evt) {
		if(!googlemaps.isRouted()) return; // check that the route is plotted
		var image_src = googlemaps.getStaticMapURL(200, 200, markers); // grab the static map at 200px x 200px and use the marker data
		$("<img>", {
			src : image_src
		}).appendTo("body");
	});

	// Initialization of the gauge
	$("#needle").rotate({
		angle:-45,
		animateTo: oldRotation
	});

	function handleMouseMove(evt) {
		//console.log("mouse move: ",evt);
		googlemaps.drawToRoad(origin, evt.latLng);
	}

	/**
	 * plotPoint
	 * Plots a marker on the map to the nearest street.
	 * Also store a reference in the markers array.
	 * @param  {Object} latLng The lngLat of the marker to plot
	 * @return {Object}        The marker that was plotted
	 */
	function plotPoint(latLng) {
		var marker = new google.maps.Marker({
			draggable: true,
			map: map
		});
		// store all the markers in an array
		// this does two things: used for count
		// and to clear the map
		markers.push(marker);
		googlemaps.getNearestStreet(latLng);
		//marker.setPosition(googlemaps.getNearestStreet(latLng));

		// marker release
		google.maps.event.addListener(marker, 'dragend', function(evt) {
			googlemaps.getNearestStreet(marker, evt.latLng);
		});

		return marker;
	}

	function handleRouteSuccess(pos) {
		console.log("pos: ",pos.data);
		//marker.setPosition(pos);
	}

	/**
	 * updateGauge
	 * Animates the position of the guage
	 * @param  {Number} distance The total disatnce travlled
	 */
	function updateGauge(distance) {
		var rotate = 0;
		if(rotate <= 45) {
			rotate = 45 - (distance * ratio);
		} else {
			rotate = (distance * ratio) - 45;
		}
		$("#needle").rotate({
			angle: oldRotation,
			animateTo: rotate
		});

		oldRotation = rotate;
	}

	/**
	 * handleMapClick
	 * Teh event handler that listens to any
	 * clicks on the map. Checks if the route has
	 * been plotted to prevent additional markers.
	 * @param  {Object} evt The event object from the map
	 */
	function handleMapClick(evt) {
		if(googlemaps.isRouted()) return;
		// store the origin point
		if (origin === null) {
			origin = evt.latLng;
			//cl = google.maps.event.addListener(map, 'mousemove', handleMouseMove);
		} else if (destination === null) {
			// store the destination
			destination = evt.latLng;
			//google.maps.event.removeListener(cl);
			googlemaps.drawToRoad(origin, evt.latLng);
			//
		} else {
			// store the waypoint data
			waypoints.push({ location: destination, stopover: true });
			// set the new end point
			destination = evt.latLng;
		}
		plotPoint(evt.latLng);
		// check if we're at the maximum marker
		if(markers.length === MAX_MARKERS) route();
	}

	/**
	 * route
	 * Plots the route on the map and
	 * calculats the distance. Also clears the
	 * map of markers to use the standard
	 * route markers.
	 */
	function route() {
		googlemaps.calcRoute(origin, destination, waypoints);
		googlemaps.calcDistance();
		googlemaps.clearOverlay(markers);
	}

	/**
	 * handleDirectionChange
	 * Checks the distance of the route
	 * to ensure yoy haven't gone too far.
	 * Undoes the previous plot point if you
	 * have.
	 * @param  {Object} evt      The event from the map
	 * @param  {Number} distance The total distance of the route
	 */
	function handleDirectionChange(evt, distance) {
		// check if we're past our max
		// only change the distance if we're not
		if(Math.ceil(distance) > MAX_DISTANCE) {
			alert("You've exceeded the maximum distance");
			googlemaps.undo();
		} else {
			$("#distance span").text(distance+" km");
			updateGauge(distance);
		}
	}
});