$(function(){
    var cookie = SNR.browser.utils.Cookie, // reference to the script obj
        cookie_name = "testCookie", // generic name
        isSet = false, // flag if the cookie is set
        $el = {
            create : $("button[name=create]"),
            read : $("button[name=read]"),
            erase : $("button[name=erase]")
        };

    function init() {
        $el.create.on("click", handleCookieCreate);
        $el.read.on("click", handleCookieRead);
        $el.erase.on("click", handleCookieErase);
    }

    /**
     * update
     * Sends the message to the screen and
     * enables/disables buttons
     * @param  {string} msg The message to display
     * @return {null}
     */
    function update(msg) {
        $("#msg").html(msg.toString());
        $el.create[0].disabled = isSet;
        $el.read[0].disabled = !isSet;
        $el.erase[0].disabled = !isSet;
    }

    /**
     * handleCookieCreate
     * Creates the cookie with a value
     * of a random string.
     * @param  {object} evt The button event
     * @return {null}
     */
    function handleCookieCreate(evt) {
        cookie.create(cookie_name, String(Math.random()*1000), 1);
        isSet = true;
        update("Cookie set");
    }

    /**
     * handleCookieRead
     * Reads the value of the cookie
     * @param  {object} evt The button event from the 'read' button
     * @return {null}
     */
    function handleCookieRead(evt) {
        var props = cookie.read(cookie_name);
        update("Cookie read: "+props);
    }

    /**
     * handleCookieErase
     * Erases the cookie data
     * @param  {object} evt The button event from the 'erase' button
     * @return {null}
     */
    function handleCookieErase(evt) {
        cookie.erase(cookie_name);
        isSet = false;
        update("Cookie erased");
    }

    init();
});