$(function(){

    var makeSpinner = function(){
        var positions = [{time:0,pos:0}],
            MAX_SPIN = 0.12,
            isDragging = false,
            velocityFactor = 0.05,
            dir = 1,
            friction = -0.0001,
            isAnimating = false,
            f = SNR.modules.FlipBook();

        function handleMouseMove(e) {
            e.preventDefault();

            if(isDragging) {
                dir = (e.clientX < positions[0].pos) ? -1 : 1;
                storePos(e.clientX);
                f.stepFrame(dir);
            }
        }

        function handleMouseUp(e) {
            calcVelocity();
            prevFPos = 0;
            if(Math.abs(vX) > 0) {
                var duration = -Math.abs(vX)/friction,
                    finalFrame = 0.5*dir*friction*duration*duration + vX*duration + f.getCurrentFrame();
                isAnimating = true;
                f.animate(finalFrame, duration);
            }
            isDragging = false;
        }

        function handleMouseDown(e) {
            e.preventDefault();

            if(isAnimating) handleAnimationComplete();
            isDragging = true;
            storePos(e.pageX);
        }

        function storePos(xPos) {
            var d = new Date(),
                track = { time: d.getSeconds() / 1000 + d.getMilliseconds(), pos: xPos };

            positions[1] = positions[0];
            positions[0] = track;
        }

        function calcVelocity() {
            vX = ((positions[0].pos - positions[1].pos) / (positions[0].time - positions[1].time))*velocityFactor;
            vX = Math.max(Math.min(vX, MAX_SPIN), -MAX_SPIN);
        }

        function handleAnimationComplete() {
            f.stop();
            isAnimating = false;
        }

        function addFrame(index) {
            f.pushFrame('360/jeep_grand_cherokee_frame_'+index+'.jpg', function(img, index){
                f.showFrame(index);
            });
        }

        // init
        var numLoaded = 0,
            curr = 0,
            last = 36;

        for(var i=1; i<= 36; i++){
            addFrame(i);
        }

        $("img").bind('mousedown', handleMouseDown)
                .bind('mouseup', handleMouseUp)
                .bind('mousemove', handleMouseMove);

        $('body button').bind('click', function(){
            var curr = f.getCurrentFrame(),
                last = (curr < f.getLength()*0.5)? 0 : 36;
            f.animate(last, Math.sqrt(-2*Math.abs(curr-last)/friction));
        });

        f.animate(last, 3000, null, 'easeOutBounce');
        f.init($("img")[0]);
    };

    makeSpinner();
});