/**
 * A core script for dispatching events to listening objects with no other dependencies.
 * Used primarily for compositions of other scripts.
 *
 * @author <a href='mailto:ntibbles@sapient.com'>Noel Tibbles</a>
 * @version 0.1
 * @requires Nothing
 *
 * Usage:
 * var dispatcher = new SNR.events.EventDispatcher();
 * dispatcher.add('event.name', handleEvent);
 * dispatcher.dispatch('event.name', {...data});
 * dispatcher.remove('event.name', handleEvent);
 *
 * Copyright (C) 2012 SapientNitro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.events = SNR.events || {};

SNR.events.EventDispatcher = function() {
    /* Private Properties _____________________________________________________________ */

    var _listeners = [];

    /* Public Methods _________________________________________________________________ */

    /**
     * Add an event handler for the specified event.
     *
     * @param {string} eventName The name of the event to handle
     * @param {function} handler The method to call when the event is triggered
     * @return {null}
     */
    function add(eventName, handler) {
        remove(eventName, handler);

        _listeners.push({eventName: eventName, handler: handler});
    }

    /**
     * Removes the event handler from the specified event.
     *
     * @param {string} eventName The name of the event to remove
     * @param {function} handler The method to remove the event from
     * @return {null}
     */
    function remove(eventName, handler) {
        if (!_listeners.length) return;

        var len = _listeners.length,
            ob = null;

        while (len--) {
            ob = _listeners[len];

            if (ob.eventName === eventName && ob.handler === handler) {
                _listeners.splice(len, 1);
                break;
            }
        }
    }

    /**
     * Removes all listeners that are currently subscribed.
     *
     * @return {null}
     */
    function removeAll() {
        _listeners = [];
    }

    /**
     * Dispatches the specified event to all its current listeners.
     *
     * @param {string} eventName The name of the event to dispatch
     * @param {object} data Any additional data to pass along to the handlers
     * @return {null}
     */
    function dispatch(eventName, data) {
        var len = _listeners.length;

        while (len--) {
            if (_listeners[len].eventName === eventName) {
                _listeners[len].handler(data);
            }
        }
    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Reports a string representation of this object.
     *
     * @return {string} Human-readable representation of this object
     * @override
     */
    function toString() {
        return "[object EventDispatcher]";
    }

    /**
     * Expose API publicly.
     */
    return {
        add: add,
        dispatch: dispatch,
        remove: remove,
        removeAll: removeAll
    };
};