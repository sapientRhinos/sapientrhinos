/**
 * Async
 * Loads images into memory synchronously or asynchronously.
 * Pass an int to the init function to load that many images
 * asynchronously. Pass the boolean false to load all the
 * images synchronously.
 *
 * @author <a href='mailto:ntibbles@sapient.com'>Noel Tibbles</a>
 * @version 0.1
 * @requires None
 *
 * Usage:
 * var loader = new SNR.loader.image.Async(false),
 *      imgs = [];
 *
 *  $.each($("img"), function(val, index) {
 *      imgs.push({ img: $(this)[0], src: $(this).data("src") });
 *  });
 *
 *  loader.init() or loader.init(3)
 *
 *  loader.addListener(loader.LOAD_SUCCESS, handleLoadSuccess);
 *  loader.addListener(loader.LOAD_COMPLETE, handleLoadComplete);
 *  loader.load(imgs);
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.loader = SNR.loader || {};
SNR.loader.image = SNR.loader.image || {};

SNR.loader.image.Async = function() {

    /* Constants ______________________________________________________________ */

    var LOAD_SUCCESS = "loadSuccess",
        LOAD_ERROR = "loadError",
        LOAD_COMPLETE = "loadComplete";

    /* Public Properties ______________________________________________________________ */

    var property = 'setting';

    /* Private Properties _____________________________________________________________ */

    var _numLoadingImages,
        _async,
        _loadingQueue,
        _p = this.prototype = new SNR.events.EventDispatcher();

    /* Public Methods _________________________________________________________________ */

    /**
     * Init
     *
     * @param {boolean} async Flag to indicate if we load asynchronously
     * @return {null}
     */
    function init(async) {
        _numLoadingImages = 0;
        _loadingQueue = [];
        _async = (async === undefined) ? true : false;
    }

    /**
     * load
     * Checks if an object or array of objects is
     * to be loaded.
     * @param  {object/array} resource An object with 'img' dom element and 'src' path to load or array of object
     * @return {null}
     */
    function load(resource) {
        if(Array.isArray(resource)) {
            while(resource.length) {
                preload(resource.shift());
            }
        } else {
            preload(resource);
        }
    }

    /* Private Methods ________________________________________________________________ */

    /**
     * preload
     * Loads the path into the img src.
     * @param {object} obj An object with 'img' dom element and 'src' path to load
     * @return {null}
     */
    function preload(obj) {
        var thread = function(){
                obj.img.onerror = function(){
                    _p.dispatch(LOAD_ERROR, {msg: "Error loading: "+url});
                    loadFinished.call(this);
                };

                obj.img.onload = function(){
                    if(obj.img.width < 1){
                        return obj.img.onerror();
                    }
                    _p.dispatch(LOAD_SUCCESS, {img: obj.img});
                    loadFinished.call(this);

                    return true;
                };
                obj.img.src = obj.src;
            };

        if(_async){
            if(typeof _async == 'number' && _async <= _numLoadingImages){
                _loadingQueue.push(function(){
                    _numLoadingImages++;
                    setTimeout(thread, 1);
                });
            } else {
                _numLoadingImages++;
                setTimeout(thread, 1);
            }
        } else {
            _numLoadingImages++;
            thread();
        }
    }

    /**
     * checkLoadComplete
     * Determines if all the images are loaded and
     * if they are, dispatchs the complete event
     * @return {null}
     */
    function checkLoadComplete() {
        if(_numLoadingImages <= 0){
           _p.dispatch(LOAD_COMPLETE);
        }
    }

    /**
     * loadFinished
     * Handles the load event from the image and
     * checks if another image is to be loaded
     * @return {null}
     */
    function loadFinished() {
        var run;
        _numLoadingImages--;
        if(typeof _async == 'number' && _async > _numLoadingImages){
            run = _loadingQueue.shift();
            if(run){
                run();
            }
        }
        checkLoadComplete();
    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Reports a string representation of this object.
     *
     * @return {string} Human-readable representation of this object
     * @override
     */
    function toString() {
        return "[object Async]";
    }

    /**
     * Expose API publicly.
     */
    return {
        LOAD_COMPLETE : LOAD_COMPLETE,
        LOAD_SUCCESS : LOAD_SUCCESS,
        LOAD_ERROR : LOAD_ERROR,
        addListener : _p.add,
        removeListener : _p.remove,
        init : init,
        load : load
    };
};