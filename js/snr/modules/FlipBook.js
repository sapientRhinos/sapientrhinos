/**
 * FlipBook
 * Combines multiple images to create a filmstrip like interaction.
 *
 * @author <a href='mailto:ntibbles@sapient.com'>Noel Tibbles</a> and <a href='mailto:jpalfree@sapient.com'>Jasper Palfree</a>
 * @version 0.1
 * @requires SNR.events.EventDispatcher, SNR.loader.image.Async, easing
 *
 * Usage:
 * var fb = new SNR.modules.FlipBook();
 * fb.init(img);
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.modules = SNR.modules || {};

SNR.modules.FlipBook = function() {

    var VERSION = 0.1;

    /* Private Properties _____________________________________________________________ */

    var _p = this.prototype = new SNR.loader.image.Async(),
        _target,
        _currFrame,
        _images = [],
        _labels = {},
        _callback,
        _animationTimer;

    /* Public Methods _________________________________________________________________ */

    /**
     * init
     *
     * @param {object} target The image element on the page
     * @param {function} callback The function to call when all images are loaded
     * @return {null}
     */
    function init(target, callback) {
        if(!window.Easing) {
            throw "Please add the easing.js script";
        }
        _target = target;
        _callback = callback;
        _p.addListener(_p.LOAD_SUCCESS, handleLoadSuccess);
        _p.init();
    }

    /*
     * pushFrame
     * Append image or url to frame set
     * @param resource - Image/string url - for image
     * @param callback (optional) - function - callback function on complete load of this resource
     * @param label (optional) - string - associate a unique label with this frame
     *
     * return FlipBook
     */
    function pushFrame(resource, callback, label){
        return setFrame(_images.length, resource, callback, label);
    }

    /*
     * setFrame
     * Set image or url for specific frame
     * @param index - int - index to set
     * @param resource - Image/string url - for image
     * @param callback (optional) - function - callback function on complete load of this resource
     * @param label (optional) - string - associate a unique label with this frame
     *
     * return FlipBook
     */
    function setFrame(index, resource, callback, label){
        if(resource instanceof Image){
            // image doesn't exist
            if(!_images[index]){
                _images[index] = new Image();
            }

            if(_currFrame === undefined){
                _currFrame = index;
            }

            if(typeof callback === 'function'){
                callback(_images[index], index);
            }

            setLabel(index, label);

            return this;
        }
        else if(typeof resource === 'string'){
            if(!_images[index]){
                _images[index] = new Image();
                setLabel(index, label);
                if(_currFrame === undefined){
                    _currFrame = index;
                }
            }
            _p.load({img : _images[index], src : resource });

            return this;
        }
        throw 'Bad resource passed to setFrame() method';
    }

    /*
     * setLabel
     * Associate a label with a frame
     * @param frame - number/string representing the index or label of a frame
     * @param label - string - label for this frame
     *
     * return FlipBook
     */
    function setLabel(frame, label){
        var f = frame;
        if((_images[frame] instanceof Image || _images[(f = _labels[label])] instanceof Image) && typeof label === 'string'){
            _labels[label] = f;
        }
        return this;
    }

    /*
     * setSource
     * Sets the source of the image element
     * @param domElement - DOM image element
     * @param callback (optional) - function - callback function on complete load of all images
     *
     * return FlipBook
     */
    function setSource(domElement){
        // check if dom element
        if( typeof HTMLElement === "object" ? domElement instanceof HTMLElement : //DOM2
            typeof domElement === "object" && domElement.nodeType === 1 && typeof domElement.nodeName==="string"
        ){
            _target.setAttribute('src', _images[_currFrame].src);
            _p.addListener(_p.LOAD_COMPLETE, callback);
        }
        return this;
    }

    /*
     * showFrame
     * Show a specific frame
     * @param frame - int/string - range:(0..length-1) index of image to show/ string label of frame
     *
     * return FlipBook
     */
    function showFrame(frame){
        var f = frame;
        if(frame === undefined){
            return this;
        }
        if((_images[frame] instanceof Image || _images[(f = _labels[frame])] instanceof Image ) && _target){
            _target.src = _images[f].src;
            _currFrame = f;
        } else {
            throw "Frame index out of range or doesn't exist.";
        }
        return this;
    }

    /*
     * stepFrame
     * Increment or decrement shown frame by step size
     * @param stepSize - int - positive or negative step size to step by
     *
     * return FlipBook
     */
    function stepFrame(stepSize){
        stepSize = ~~stepSize;
        if(stepSize === 0){
            return this;
        } else if(stepSize > 0){
            showFrame((_currFrame + stepSize) % getLength());
        } else {
            var next = (_currFrame + stepSize) % getLength();
            next += next < 0? getLength() : 0;
            showFrame(next);
        }
        return this;
    }

    /*
     * Show next frame
     *
     * return FlipBook
     */
    function nextFrame(){
        stepFrame(1);
        return this;
    }

    /*
     * Show previous frame
     *
     * return FlipBook
     */
    function prevFrame(){
        stepFrame(-1);
        return this;
    }

    /*
     * Get the image object of specified frame
     * @param - frame - int/string: index or label of image
     *
     * return Image
     */
    function getFrameImage(frame){
        var img = null;
        if(frame === undefined){
            return null;
        }
        if((img = _images[frame]) instanceof Image || (img = _images[_labels[frame]]) instanceof Image ){
            return img;
        } else {
            return null;
        }
    }

    /*
     * Animate the changing of frames using easing functions
     * @param endIndex (optional) - int - index to end animation at
     * @param duration (optional) - int - animation duration in milliseconds
     * @param callback (optional) - function - callback function on animation complete
     * @param easing (optional) - string - penner easing function to use
     *
     * return FlipBook
     */
    function animate(endIndex, duration, callback, easing){
        easing = easing || window.Easing['def'];
        endIndex = ~~endIndex; //dumb int parse
        duration = ~~duration;
        var begin = _currFrame,
            change = endIndex-begin,
            interval = Math.abs(duration/change), //ms
            time = 0,
            next = window.Easing[easing],
            len = getLength();

        if(typeof next === 'function'){
            var showNextFrame = function(){
                _animationTimer = setTimeout(function(){
                    var nextFrame;
                    time += interval;
                    if(time <= duration){
                        nextFrame = Math.floor(next(_currFrame, time, begin, change, duration) % len);
                        nextFrame += (nextFrame < 0)? len : 0;
                        showFrame(nextFrame);
                        showNextFrame();
                    } else {
                        nextFrame = Math.floor(endIndex % len);
                        nextFrame += (nextFrame < 0)? len : 0;
                        showFrame(nextFrame);
                        if(typeof callback === 'function'){
                            callback(me);
                        }
                    }
                }, interval);
            };

            showNextFrame();
        }
        return this;
    }

    /*
     * Stop animating
     *
     * return FlipBook
     */
    function stop(){
        clearInterval(_animationTimer);
        return this;
    }

    /*
     * Get DOM element of frame switcher
     *
     * return DOM Element of type <img />
     */
    function getElement(){
        return _target;
    }

    /*
     * Get the current frame
     *
     * return DOM Element of type <img />
     */
    function getCurrentFrame(){
        return _currFrame;
    }

    /*
     * Get number of frames
     *
     * return int
     */
    function getLength(){
        return _images.length;
    }

    /* Private Methods ________________________________________________________________ */

    /**
     * Method description.
     *
     * @param {string} paramName Short description of what purpose the param serves
     * @return {null}
     */
    function handleLoadSuccess(img) {
        if(typeof _callback === 'function'){
            _callback(img, index);
        }
    }

    /*
     * Map coordinate value to frames
     * @param coord - float - coordinate
     * @param interval - num coordinates "between" frames
     *
     * return FlipBook
     */
    function mapCoordToFrame(coord, interval){
        var s = ~~interval || 1,
            newFrame = Math.floor((coord/s) % getLength());
        if(_currFrame != newFrame){
            showFrame(newFrame);
        }
        return this;
    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Reports a string representation of this object.
     *
     * @return {string} Human-readable representation of this object
     * @override
     */
    function toString() {
        return "[object FlipBook]";
    }

    /**
     * Expose API publicly.
     */
    return {
        VERSION : VERSION,
        init : init,
        pushFrame : pushFrame,
        setFrame : setFrame,
        setLabel : setLabel,
        setSource : setSource,
        showFrame : showFrame,
        stepFrame : stepFrame,
        nextFrame : nextFrame,
        prevFrame : prevFrame,
        getCurrentFrame : getCurrentFrame,
        getFrameImage : getFrameImage,
        getLength : getLength,
        animate : animate,
        stop : stop,
        getElement : getElement,
        addListener : _p.addListener,
        removeListener : _p.removeListener
    };
};