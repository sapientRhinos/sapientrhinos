/**
 * GoogleMaps.twitter - SNR.google.maps extension for plotting tweets
 * @author Brett Kellgren
 *
 * Dependancies:
 * jQuery - <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 * Google Maps - <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
 * Google Loader - <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 * SNR.google.maps.utils - <script type="text/javascript" src="/js/snr/google/maps/utils.js"></script>
 * SNR.google.maps.libs.markercluster - <script type="text/javascript" src="/js/snr/google/maps/libs/markercluster.js"></script> (COMMENTED OUT FOR NOW)
 * SNR.google.maps.libs.infobox - <script type="text/javascript" src="/js/snr/google/maps/libs/infobox.js"></script>
 * simpleCache.php - php/libs/simpleCache.php
 * cacheProxy.php - php/cacheProxy.php
 *
 * Usage:
 * SNR.google.maps.twitter.addTweets(map, '#livingthelife');
 *
 * Copyright (C) 2012 SapientNitro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.google = SNR.google || {};
SNR.google.maps = SNR.google.maps || {};

SNR.google.maps.twitter = (function($) {

    /*____________________ CONSTANTS ____________________*/

    PROXY_URL = '/php/cacheProxy.php';

    /*____________________ PUBLIC METHODS ____________________*/

    /**
     * addTweets
     * add tweets to an existing map
     * @param  {Object} map <required> The existing google Map to apply users
     * @param {Object} tweets <required> Supplies tweets to include (JSON from twitter api)
     */
    function addTweets(map, tweets) {
        if(!tweets) {
            throw "Must supply at least one tweet";
        }
        var handles = [];
        for(tweet in tweets){
            handles.push(tweets[tweet].from_user);
        }
        $.post(PROXY_URL, {
            url: 'http://api.twitter.com/1/users/lookup.json?screen_name=' + handles.join(),
            cacheLabel: 'twitterUsers'
        }, function(users) {
            users = $.parseJSON(users);
            var addresses = '';
            for (i in users){
                if(users[i].location && (users[i].location !== '')){
                    addresses += '&location='+escape(users[i].location);
                    tweets[i].userDetail = users[i];
                }
            }
            geocode(addresses, function(tweets){
                var markers = []
                highestZindex = 2,
                ibArr=[];
                for(i in tweets){
                    var tweet = tweets[i];
                    if(typeof(tweet.userDetail) != "undefined"){
                        if (typeof(tweet.latLng) != "undefined"){
                            var marker = new google.maps.Marker({
                                icon: tweet.userDetail.profile_image_url,
                                title: tweet.userDetail.screen_name,
                                position: new google.maps.LatLng(tweet.latLng.lat, tweet.latLng.lng),
                                zIndex: 1,
                                map: map
                            });
                            var boxText = document.createElement("div");
                            // TODO: Create options for these and the boxStyle below
                            boxText.style.cssText = "border:3px solid #666; width: 250px;background: #424242;padding: 10px 5px 5px;margin:10px;color: #fff;font-size:.9em; text-shadow: black 0.1em 0.1em 0.2em;";
                            boxText.innerHTML = tweet.created_at + "<br>" + tweet.text;

                            // TODO: accept style options as params

                            var myOptions = {
                                content: boxText,
                                disableAutoPan: false,
                                maxWidth: 0,
                                pixelOffset: new google.maps.Size(-140, 0),
                                zIndex: null,
                                boxStyle: {
                                  opacity: 0.9,
                                  width: "280px"
                                },
                                closeBoxMargin: "10px 2px 2px 2px",
                                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                                infoBoxClearance: new google.maps.Size(1, 1),
                                isHidden: false,
                                pane: "floatPane",
                                enableEventPropagation: false
                            };

                            ibArr[tweet.from_user] = new InfoBox(myOptions);
                            var ib = ibArr[tweet.from_user];
                            (function(ibArr, ib){
                                google.maps.event.addListener(marker, "click", function (e) {
                                    for(i in ibArr){
                                        ibArr[i].close();
                                    }
                                    ib.open(map, this);
                                    this.setZIndex(highestZindex);
                                    highestZindex = highestZindex++;
                                });
                            })(ibArr, ib);


                            //markers.push(marker);
                        }
                    }
                }
                //var markerCluster = new MarkerClusterer(map, markers);

            }, tweets);

        });
    }

    /**
     * search
     * add tweets to map related to search query
     * @param  {Object} map <required> The existing google Map to apply users
     * @param {String} query <required> search string (i.e. jeep, #jeep or @jeep) 1000 char maximum
     *
     */
    // TODO: accept cachePath and cacheSeconds as params
    function search(map, query) {

        if(!query) {
            throw "Must supply a search string (i.e. jeep, #jeep or @jeep)";
        }
        $.post(PROXY_URL,  {
            url: 'http://search.twitter.com/search.json?q=' + escape(query) +'&geocode=37.6970,-91.8096,3000mi&rpp=100',
            cacheLabel: 'twitterSearch'
        }, function(tweets) {
            tweets = $.parseJSON(tweets);
            addTweets(map, tweets.results);
        });
    }

    /* Private Methods ________________________________________________________________ */

    /**
     * geocode
     * Uses mapquest's geocoding api to convert a text address
     * into Lat/Lng
     * @param  {STRING} addresses <required> a GET string with comma separated addresses i.e. &location=123%20St%20Chicago%20IL
     * @param  {FUNCTION} callback callback function
     * @param {Object} tweets to be geocoded and returned with latLng property
     * @return {Object} tweets object modified to include latLng
     */
    function geocode(addresses, callback, tweets) {


        // google


        // mapquest
        $.post(PROXY_URL,  {
            url: 'http://www.mapquestapi.com/geocoding/v1/batch?key=Fmjtd%7Cluuanu6anh%2C82%3Do5-96agur&format=json'+addresses.replace('.', ''),
            cacheLabel: 'mapquestGeocode'
        }, function(response){
            response = $.parseJSON(response);
            console.log(response);
            if (!response.info.statuscode) {
                for(i in response.results){
                    tweets[i].latLng = response.results[i].locations[0].latLng
                }
                if(callback){
                    callback(tweets);
                }
                return tweets;
            }else{
                throw 'MapQuest: '+ response.info.statuscode + ': ' + response.info.messages[0];
            }

        });
    }

    /*____________________ PUBLICLY EXPOSE API ____________________*/

    return {
        search: search
    };
})(jQuery);