/**
 * A description of your class goes here.
 * Documentation should be written as per the Google Closure Compiler annotation doc (https://developers.google.com/closure/compiler/docs/js-for-compiler)
 * Furthermore, tags can be used as seen on that page as well as the JSDoc toolkit documentation (http://code.google.com/p/jsdoc-toolkit/wiki/TagReference)
 *
 * @author <a href='mailto:email@sapient.com'>Your Name</a>
 * @version 0.1
 * @requires List any dependencies here
 *
 * Usage:
 * var bp = new SNR.Boilerplate();
 * bp.publicMethod('test');
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.loader = SNR.loader || {};
SNR.loader.image = SNR.loader.image || {};

SNR.loader.image.async = function() {
    /* Public Properties ______________________________________________________________ */

    var property = 'setting';

    /* Private Properties _____________________________________________________________ */

    var _fullLoadCallback,
        _numLoadingImages,
        _async,
        _loadingQueue;

    /* Public Methods _________________________________________________________________ */

    /**
     * Method description.
     *
     * @param {string} paramName Short description of what purpose the param serves
     * @return {null}
     */
    function init(settings) {
        _fullLoadCallback = null;
        _numLoadingImages = 0;
        _loadingQueue = [];
        _async = (settings.async === undefined)? true : settings.async;
    }

    /* Private Methods ________________________________________________________________ */

    /**
     * Method description.
     *
     * @param {string} paramName Short description of what purpose the param serves
     * @return {null}
     */
    function preload(img, url, async, callback) {
        //
    }

    function checkLoadComplete() {
        if(typeof _fullLoadCallback === 'function' && _numLoadingImages <= 0){
            _fullLoadCallback();
        }
    }

    function loadFinished() {
        var run;
        _numLoadingImages--;
        if(typeof this._async == 'number' && this._async > this._numLoadingImages){
            run = this._loadingQueue.shift();
            if(run){
                run();
            }
        }
        this._checkLoadComplete();
    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Reports a string representation of this object.
     *
     * @return {string} Human-readable representation of this object
     * @override
     */
    function toString() {
        return "[object Boilerplate]";
    }

    /**
     * Expose API publicly.
     */
    return {
        publicMethod: publicMethod
    };
};