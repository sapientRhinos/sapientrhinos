/**
 * The main router of the application that redirects to the appropriate page based on URL hash.
 *
 * @type {Backbone.Router}
 */
App.Workspace = Backbone.Router.extend({
    routes: {
        '': 'showDefault'
    },

    /**
     * Executed immediately when creating a new instance. Starts the history.
     *
     * @return {null}
     */
    initialize: function() {
        this.appView = new App.AppView();

        Backbone.history.start();
    },

    /**
     * Creates and transitions to the SampleView.
     *
     * @return {null}
     */
    showDefault: function() {
        this.sampleView = new App.SampleView({collection: new Samples()});
        this.appView.showView(this.sampleView);
    }
});