var App = App || {};

/**
 * Begins the routing of the application on document ready.
 */
$(document).ready(function() {
    // create a pub/sub system for our (http://lostechies.com/derickbailey/2011/07/19/references-routing-and-the-event-aggregator-coordinating-views-in-backbone-js/)
    App.Dispatcher = _.extend({}, Backbone.Events);

    new App.Workspace();
});