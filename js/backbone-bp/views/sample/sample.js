App.SampleView = BaseView.extend({
    tagName: 'section',
    id: 'home',
    template: _.template($('#sample-template').html()),

    events: {
        'click .btn-sample': 'showSample'
    },

    /**
     *
     */
    initialize: function() {
        BaseView.prototype.initialize.call(this);

        this.collection.on('change', this.render, this);
    },

    /**
     *
     */
    render: function() {
        this.$el.html(this.template());

        return this;
    },

    /**
     *
     */
    showSample: function(evt) {
        evt.preventDefault();
    }

    /**
     *
     */
    transitionOut: function(cb, scope, params) {
        // overwrite here if necessary to do some other type of animation

        BaseView.prototype.transitionOut.call(this, cb, scope, params);
    }
});