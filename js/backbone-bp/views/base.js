/**
 * A base view utility to add transitioning capabilities to views.  Extend BaseView in your views
 * rather than extending Backbone.View.  All methods here can be overwritten on a per view basis
 * to control how views are initialized and transitioned.
 *
 * Usage:
 * var SampleView = BaseView.extend({
 *     initialize: function(){
 *
 *     }
 * });
 *
 * // create a view
 * var sampleView = new SampleView({model: some_model, collection: some_collection});
 *
 * // clean up the view and remove it from the DOM
 * sampleView.clean();
 *
 * @param {object} options The view's options to set by default (same as Backbone.View's initialization options - optional).
 */
var BaseView = function (options) {
    Backbone.View.apply(this, [options]);
};

_.extend(BaseView.prototype, Backbone.View.prototype, {
    /**
     * Executed immediately when creating a new instance. Hides the containing element so that we can use
     * the transitioning methods to show it.
     *
     * @return {null}
     */
    initialize: function() {
        this.$el.hide();
    },

    /**
     * Transitions the view in. Overwrite for more complex transitions.
     *
     * @return {null}
     */
    transitionIn: function() {
        this.$el.show(500);
    },

    /**
     * Transitions the view out.  Overwrite for more complex transitions.
     *
     * @param  {Function} cb     The callback function to run after the transition completes
     * @param  {object}   scope  The scope to pass to the callback
     * @param  {array}   params  The parameters, as an array, to pass to the callback
     *
     * @return {null}
     */
    transitionOut: function(cb, scope, params) {
        this.$el.hide(500, function() {
            if (typeof cb === 'function') {
                cb.apply(scope, params);
            }
        });
    },

    /**
     * Cleans up the view.  Unbinds its events and removes it from the DOM.
     *
     * @return {null}
     */
    clean: function() {
        this.undelegateEvents();
        this.remove();
    }
});

BaseView.extend = Backbone.View.extend;