/**
 * Example taken from http://lostechies.com/derickbailey/2011/09/15/zombies-run-managing-page-transitions-in-backbone-apps/
 * Used to manage transitions between views.
 *
 * @requires jquery.scrollTo (/shared/libs/jquery/plugins/jquery.scrollto.1.0.js)
 */
App.AppView = BaseView.extend({
    el: '#sections',

    /**
     * Executed immediately when creating a new instance.
     *
     * @return {null}
     */
    initialize: function() {
        // override the super so that our app view is not hidden initially
    },

    /**
     * If a current view exists it transitions out and then shows the new view accordingly.
     *
     * @param  {BaseView} view The view to be displayed
     *
     * @return {null}
     */
    showView: function(view) {
        this.newView = view;

        if (this.currentView) {
            this.currentView.transitionOut(this.showNext, this);
        } else {
            this.showNext();
        }
    },

    /**
     * Cleans up the old current view (if one was present) and renders the new view.  Transitions the new view in
     * and scrolls to the top of the page to avoid being "stuck" at the location of where the call came from.
     *
     * @return {null}
     */
    showNext: function() {
        if (this.currentView) this.currentView.clean();
        this.currentView = this.newView;
        this.$el.html(this.currentView.render().el);
        this.currentView.transitionIn();

        // when switching main views scroll to the top of the page
        $('html, body').scrollTo();
    }
});