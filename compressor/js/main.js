/**
 * Main script driving the form page.
 * @author <a href='mailto:ntibbles@sapient.com'>Noel Tibbles</a>
 */

// Add-on to serialize arrays into JSON objects
$.fn.serializeObject = function(){
    var o = {},
        a = this.serializeArray();

    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function() {
    var cookie = SNR.browser.utils.Cookie,
        cookie_name = "userBuild",
        history = [];

    $("input[type=submit]").on("click", handleFormSubmit);
    $("#script_data").delegate("input[type=checkbox]", "click", handleCheckBoxClick);
    $("#script_names").delegate("input[type=radio]", "click", handleRadioClick);
    $("#script_names").delegate("a", "click", handleRemoveClick);
    $("#output_name").on("keyup change", validate);

    /**
     * init
     * Loads the JSON object and generates a checkbox
     * for each item.
     * @return {null}
     */
    function init() {
        var cookie_data = cookie.read(cookie_name);
        $.getJSON("data/dependencies.json", function(data){
            for(var i = 0, len = data.scripts.length; i < len; i++) {
                generateCheckBox(data.scripts[i]);
            }
        });

        if(cookie_data) {
            generatePrevBuildItem(JSON.parse(cookie_data));
        }
    }

    /**
     * generateCheckBox
     * Generates the markup for the input
     * type of checkbox.
     * @param  {object} obj An object with id, path and dependencies
     * @return {null}
     */
    function generateCheckBox(obj) {
        var div = $("<div>", {
                "class" : "cbHolder"
            }),
            cbDiv = $("<div>", {
                "class" : "squaredOne"
            }),
            cb = $("<input>", {
                type : "checkbox",
                name : "scripts[]",
                id : obj.id,
                value : obj.path,
                "data-dependents" : obj.dependencies
            }),
            label = $("<label>", {
                "for" : obj.id
            }),
            p = $("<p>").html(obj.name);
        $(cb).appendTo(cbDiv);
        $(label).appendTo(cbDiv);
        $(cbDiv).appendTo(div);
        $(p).appendTo(div);
        $(div).appendTo($("#script_data"));
    }

    /**
     * generatePrevBuildItem
     * Create the copy and the radio button for
     * any itmes stored in the array.
     * @param  {array} items The items to stored in the array
     * @return {null}
     */
    function generatePrevBuildItem(items) {
        var $pb = $("#prev_builds");
        history = items;

        $.each(history, function(index, val){
            var script_array = $(this)[0]["scripts[]"];
                div = $("<div>", {
                    "class" : "item"
                }),
                p = $("<p>").text($(this)[0]["output_name"]),
                rb = $("<input>", {
                    type : "radio",
                    name : "prevBuilds",
                    value : $.isArray(script_array) ? script_array.join() : script_array
                }),
                a = $("<a>", {
                    href : "#",
                    "data-index" : index
                }).html("remove");

            $(rb).appendTo(div);
            $(p).appendTo(div);
            $(a).appendTo(div);
            $(div).appendTo($("#script_names"));
        });

        $pb.show();
    }

    /**
     * validate
     * Basic validation to ensure at least one
     * checkbox is checked and the filename is
     * entered.
     * @param  {object} evt [Optional] Passed on change of the text input
     * @return {null}
     */
    function validate(evt) {
        var checkboxes = $("input[type=checkbox]");
        if(checkboxes.is(":checked") && $("#output_name").val() !== "") {
            $("input[type=submit]").attr("disabled", false);
        } else {
            $("input[type=submit]").attr("disabled", true);
        }
    }

    /**
     * saveData
     * Saves array data to the cookie
     * by creating JSON object from it.
     * @param  {array} items The items to save to the cookie
     * @return {null}
     */
    function saveData(items) {
        cookie.erase(cookie_name);
        cookie.create(cookie_name, JSON.stringify(history));
    }

    /**
     * handleCheckBoxClick
     * Handles when a checkbox is clicked and
     * checks any dependents that are identified
     * in data-dependents.
     * @param  {object} evt The event from the checkbox
     * @return {null}
     */
    function handleCheckBoxClick(evt) {
        if($(this).is(":checked")) {
            validate();
            var dep = $(this).data("dependents");
            if(dep === undefined) return;

            var depEl = dep.split(",");
            for(var i = 0, len = depEl.length; i < len; i++) {
                $("#"+depEl[i]).attr("checked" , "checked");
            }
        }
    }

     /**
     * handleRadioClick
     * If previous builds are present, this handles
     * all the clicks from the radio buttons to
     * regenerate it. It checks the old boxes and
     * enters the old filename.
     * @param  {object} evt The radio button event
     * @return {null}
     */
    function handleRadioClick(evt) {
        var scripts = $(this).val().split(",");
        $("input[type=checkbox]").removeAttr("checked");

        $("#output_name").val($(this).parent().clone().children().remove().end().text());

        $.each(scripts, function(index, val) {
            $("input[type='checkbox'][value='"+val+"']").attr("checked", "checked");
        });
    }

    /**
     * handleRemoveClick
     * Handles removing a previous build from
     * the display list and the cookie data.
     * @param  {object} evt The event from the remove anchor
     * @return {null}
     */
    function handleRemoveClick(evt) {
        history.splice($(this).data("index"), 1);
        $("#script_names").html("");
        generatePrevBuildItem(history);
        saveData(history);

        if(!history.length) {
            cookie.erase(cookie_name);
            $("#prev_builds").hide();
        }
    }

    /**
     * handleFormSubmit
     * Handles the form submission through an
     * AJAX request to compress.php. Shows the
     * download link when successful.
     * @param  {object} evt The click event from the submit button
     * @return {null}
     */
    function handleFormSubmit(evt) {
        evt.preventDefault();
        var filename = $("#output_name").val();

        $.post("compress.php", $("#compress_scripts").serialize(), function(data) {
            $("#download a").attr("href", "output/"+filename).html(filename);
            $("#download").show();
        });

        history.push($("#compress_scripts").serializeObject());
        if(history.length >= 4) {
           history.splice(0, 1);
        }

        saveData(history);
    }

    init();
});