/**
 * Cookies
 * A utility that handles creating, reading, and removing browser based cookies.
 * Refactored version of Quirksmode Cookies script <a href="http://www.quirksmode.org/js/cookies.html">http://www.quirksmode.org/js/cookies.html</a>
 *
 * @author <a href='mailto:email@sapient.com'>Noel Tibbles</a>
 * @version 0.1
 * @requires None
 *
 * Usage:
 * var cookie = SNR.browser.utils.Cookies;
 * cookie.create("myCookie", "userData", 1);
 * cookie.read("myCookie");
 * cookie.erase("myCookie");
 *
 * Copyright (C) 2012 SapientNitro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var SNR = SNR || {};
SNR.browser = SNR.browser || {};
SNR.browser.utils = SNR.browser.utils || {};

SNR.browser.utils.Cookies = (function() {

    /* Public Properties ______________________________________________________________ */

    var VERSION = 0.1;

    /* Public Methods _________________________________________________________________ */

    /**
     * Create
     * Creates a browser based cookie using the
     * specified name, value and expiry date.
     *
     * @param {string} name The name of the cookie
     * @param {string} value The value to set
     * @param {number} days The number of days to set to expiry the cookie
     * @return {null}
     */
    function create(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();

            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }
        document.cookie = name+"="+value+expires+"; path=/";
    }

    /**
     * read
     * Reads the value of a cookie with the
     * supplied name
     * @param  {string} name The name of the cookie ot read
     * @return {string/null} The values of the cookie or null
     */
    function read(name) {
        var nameEQ = name + "=",
            ca = document.cookie.split(';');

        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    /**
     * erase
     * Deletes the cookie of the specified name.
     * @param  {string} name The name of the cookie to remove
     * @return {null}
     */
    function erase(name) {
        create(name,"",-1);
    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Reports a string representation of this object.
     *
     * @return {string} Human-readable representation of this object
     * @override
     */
    function toString() {
        return "[object Cookie]";
    }

    /**
     * Expose API publicly.
     */
    return {
        version : VERSION,
        create : create,
        read : read,
        erase : erase
    };
}());