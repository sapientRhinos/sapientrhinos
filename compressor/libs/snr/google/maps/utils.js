/**
 * GoogleMaps - A utility wrapper for Google Maps.
 * @author Noel Tibbles
 *
 * Dependencies:
 * jQuery - <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 * Google Maps - <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
 * Google Loader - <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 *
 * Usage:
 * var renderer = new google.maps.DirectionsRenderer(),
 *		map = new google.maps.Map($('#map_canvas')[0], {
 *				zoom: 14,
 *				center: new google.maps.LatLng(43.652527,-79.381961),
 *				mapTypeId: google.maps.MapTypeId.ROADMAP
 *		});
 *
 * GoogleMaps.init({ map: map, directionsRenderer : renderer });
 * GoogleMaps.getNearestStreet(marker, latLng);
 */
var SNR = SNR || {};
SNR.google = SNR.google || {};
SNR.google.maps = SNR.google.maps || {};

SNR.google.maps.utils = (function($) {

	/*____________________ VARIABLES ____________________*/
	var $defaults = {
			map : null,
			directionsRenderer : null
		},
		distanceService,
		directionsService,
		elevationService,
		currentDirections,
		prevDirections,
		isRouted,

	/*____________________ CONSTANTS ____________________*/

		VERSION = 0.32,
		ELEVATION_SUCCESS = "elevationSuccess",
		ELEVATION_FAILURE = "elevationFailure",
		GEOLOCATION_SUCCESS = "geolocationSuccess",
		GEOLOCATION_FAILURE = "geolocationFail",
		DISTANCE_CHANGED = "distanceChanged";

	/*____________________ INITIALIZATION ____________________*/
	
	/**
	 * initialize
	 * Initializes the GoogleMaps object. Checks for options
	 * and initializes all dependencies.
	 * @param {Object} options <required> Passes references to Map and DirectionsRenderer
	 */
	function initialize(options) {
		if(!options || !options.map || !options.directionsRenderer) {
			throw "Options with google.maps.Map and google.maps.DirectionsRenderer are required.";
		}

		$.extend($defaults, options);

		prevDirections = [];
		currentDirections = null;
		isRouted = false;
		directionsService = new google.maps.DirectionsService();
		distanceService = new google.maps.DistanceMatrixService();
		elevationService = new google.maps.ElevationService();
		initMap();
		initEventHandlers();
	}

	/**
	 * initMap
	 * Passes the DirectionRenderer options required
	 * for GoogleMaps to work correctly.
	 */
	function initMap() {
		$defaults.directionsRenderer.setOptions({
			'map': $defaults.map,
			'preserveViewport': true,
			'draggable': true
		});
	}

	/**
	 * initEventHandlers
	 * Initializes all the event handlers
	 */
	function initEventHandlers() {
		google.maps.event.addListener($defaults.directionsRenderer, 'directions_changed', function(){
			handleDirectionChange($defaults.directionsRenderer.directions);
		});
	}

	/*____________________ PUBLIC METHODS ____________________*/

	/**
	 * geolocate
	 * Uses the HTML5 geoloaction object to try and location
	 * the user. If it fails, fallsback to the geolocateFallback
	 * method.
	 */
	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position){
				$(document).trigger(GEOLOCATION_SUCCESS, new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
			}, geoLocateFallback);
		} else {
			geoLocateFallback();
		}
	}

	/**
	 * getStaticMapURL
	 * Generates the static map URL based on the
	 * parameter in the current map
	 * @param  {[Number} width   The width of the map in pixels
	 * @param  {Number} height  The height of the map in pixels
	 * @param  {Array} markers An array of all the markers on the map
	 * @return {String}         The URL of the static map
	 */
	function getStaticMapURL(width, height, markers) {
		var path_poly = $defaults.directionsRenderer.directions.routes[0].overview_polyline.points,
			map_center = $defaults.map.getCenter(),
			parameters = {
				center : map_center.Xa+","+map_center.Ya,
				markers : encodeMapPoints(markers),
				path : "enc:"+path_poly,
				size : width+"x"+height,
				sensor : "false"
			};

		return "http://maps.googleapis.com/maps/api/staticmap?"+$.param(parameters);
	}

	/**
	 * getIsRouted
	 * Getter to determine if the route
	 * has been drwn to the map
	 * @return {Boolean} True if it's drawn
	 */
	function getIsRouted() {
		return isRouted;
	}

	/**
	 * getNearestStreet
	 * Gets the closest street to the marker location
	 * and sets it's position.
	 * @param  {Object} marker <required> The google Marker being placed
	 * @param  {Object} latLng <required> The google LatLng object associated with the marker
	 */
	function getNearestStreet(marker, latLng) {
		var request = {
				origin : latLng,
				destination : latLng,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

		directionsService.route(request, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				marker.setPosition(response.routes[0].legs[0].end_location);
			} else {
				throw "An error occurred: "+status;
			}
		});
	}

	/**
	 * getElevation
	 * Gets theelevation of a single point on the map
	 * and triggers and event.
	 * @param  {Object} latLng <required> A google map latLng object
	 * @return {Number|String} Will return the elevation on success or a failure string
	 */
	function getElevation(latLng) {
		var location = [latLng],
			request = {
				'locations': location
			};

		elevationService.getElevationForLocations(request, function(results, status) {
			if (status === google.maps.ElevationStatus.OK) {
				if (results[0]) {
					$(document).trigger(ELEVATION_SUCCESS, results[0].elevation);
				} else {
					$(document).trigger(ELEVATION_FAILURE, status);
				}
			}
		});
	}

	/**
	 * calcRoute
	 * Determines the optimum driving route to take from the beginning
	 * of the route to the end. Optional waypoints can be added.
	 * @param {Object} start <required> A lngLat object from the marker
	 * @param {Object} end   <required> A lngLat object from the marker
	 * @param {Array} waypoints <optional> An arrays of markers (waypoints) to plot
	 */
	function calcRoute(start, end, waypoints) {
		var request = {
				origin:start,
				destination:end,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

		if(waypoints) {
			request.waypoints = waypoints;
			request.optimizeWaypoints = true;
		}

		directionsService.route(request, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				$defaults.directionsRenderer.setDirections(response);
				isRouted = true;
			} else {
				throw "An error occurred: "+status;
			}
		});
	}

	/**
	 * calcDistance
	 * Gets the current route and calculates the distance
	 * for the entire route. It then dispatches an event
	 * with a distance object.
	 * @return {Object} distance object with 'value' and 'text'
	 */
	function calcDistance(directions) {
		if(directions === undefined) { return; }
		var total = 0,
			curRoute = directions.routes[0];

		for (var i = 0, len = curRoute.legs.length; i < len; i++) {
			total += curRoute.legs[i].distance.value;
		}
		total = total / 1000;

		$(document).trigger(DISTANCE_CHANGED, total);
	}

	/**
	 * undo
	 * Undoes the current route and replaces it with the
	 * previous route.
	 */
	function undo() {
		currentDirections = null;
		if (prevDirections.length) {
			$defaults.directionsRenderer.setDirections(prevDirections.pop());
		}
	}

	/*____________________ PRIVATE METHODS ____________________*/

	/**
	 * geoLocateFallback
	 * @private
	 * The fallback for when the html5 geolocation fails.
	 * This uses Google's loader to try and determine the location
	 * based on the user's IP.
	 * Dispatches an event on success and fail.
	 * @return {Object|Boolean} Dispatches success with lngLat or Boolean on fail.
	 */
	function geoLocateFallback() {
		var loc = {};

		if(google.loader.ClientLocation) {
			loc.lat = google.loader.ClientLocation.latitude;
			loc.lng = google.loader.ClientLocation.longitude;
			$(document).trigger(GEOLOCATION_SUCCESS, new google.maps.LatLng(loc.lat, loc.lng));
		} else {
			$(document).trigger(GEOLOCATION_FAIL);
		}
	}

	/**
	 * encodeMapPoints
	 * @private
	 * URL encodes the marker points for
	 * passing to Google
	 * @param  {Array} points The point to encode with LongLat
	 * @return {String}        The URL encoded string
	 */
	function encodeMapPoints(points) {
		var ret = "";
		$.each(points, function(index, val){
			ret += val.position.Xa+","+val.position.Ya;
			if(index < points.length-1) ret += "|";
		});

		return ret;
	}

	/*____________________ EVENT HANDLERS ____________________*/

	/**
	 * handleDirectionChange
	 * Primarily used for @method undo. This stores the previous route.
	 * Then calls @method calcDistance to get the current distance.
	 * @param  {Object} direction <required> The directions from the directionsRenderer.
	 */
	function handleDirectionChange(directions) {
		if (currentDirections) {
			prevDirections.push(currentDirections);
        }
        currentDirections = directions;
        calcDistance(directions);
	}

	/*____________________ HELPERS ____________________*/

	/**
	 * clearOverlay
	 * Hides all the current markers on the map.
	 * @param  {Array} markers The current markers on the map.
	 */
	function clearOverlay(markers) {
		for (var i in markers) {
			markers[i].setMap(null);
		}
	}

	/*____________________ OVERRIDES ____________________*/

	/**
	 * toString
	 * Overrides the default toString with
	 * the name of this Object
	 * @return {String} The name of this Object
	 */
	function toString() {
		return '[object GoogleMaps]';
	}

	/*____________________ PUBLICLY EXPOSE API ____________________*/

	return {
		init : initialize,
		isRouted : getIsRouted,
		getNearestStreet : getNearestStreet,
		getElevation : getElevation,
		getStaticMapURL : getStaticMapURL,
		calcDistance : calcDistance,
		calcRoute : calcRoute,
		geolocate : geolocate,
		clearOverlay : clearOverlay,
		undo : undo,
		toString : toString,
		ELEVATION_SUCCESS : ELEVATION_SUCCESS,
		ELEVATION_FAILURE : ELEVATION_FAILURE,
		GEOLOCATION_FAILURE : GEOLOCATION_FAILURE,
		GEOLOCATION_SUCCESS : GEOLOCATION_SUCCESS,
		DISTANCE_CHANGED : DISTANCE_CHANGED
	};
})(jQuery);