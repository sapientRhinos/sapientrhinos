<?php
// java -jar yuicompressor-2.4.7.jar myfile.js -o myfile-min.js --charset utf-8
function buildScript() {
    $cmd = "";

    foreach($_POST['scripts'] as $script) {
        $cmd .= " ".$script;
    }

    compress($cmd);
}

function compress($cmd) {
    $filename = $_POST['output_name'];
    $scripts = str_replace(" --js=", ", ", $cmd);
	exec("java -jar bin/yuicompressor-2.4.7.jar".$cmd." -o output/".$filename." --charset utf-8");

    // write the scripts compressed to file
    $myFile = "output/".$filename;
    $fh = fopen($myFile, 'a') or die("can't open file");
    $stringData = "// scripts compressed: ".substr($scripts,1);
    fwrite($fh, $stringData);
    fclose($fh);
}

buildScript();
?>