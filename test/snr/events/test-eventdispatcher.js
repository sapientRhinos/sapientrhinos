/**
 * Test case suite for event dispatcher.
 *
 * @author <a href='mailto:jhok@sapient.com'>Johnathan Hok</a>
 *
 * Copyright (C) 2012 SapientNitro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var main = (function($, document, window, undefined) {
    var dispatcher;

    /**
     *
     */
    function init() {

        module('EventDispatcher.add()', {
            setup: function() {
                dispatcher = new SNR.events.EventDispatcher();
            }
        });
            test('event with handler', function() {
                var eventName = 'simple';

                dispatcher.add(eventName, function() {});
                ok(true, 'added event');
            });
            test('event with handler accepting data', function() {
                var eventName = 'simple with data';

                dispatcher.add(eventName, function(data) {});
                ok(true, 'added event');
            });
            test('events with the same name', function() {
                var eventName = 'same events';

                dispatcher.add(eventName, function() {});
                dispatcher.add(eventName, function() {});
                ok(true, 'added events');
            });

        module('EventDispatcher.dispatch()', {
            setup: function() {
                dispatcher = new SNR.events.EventDispatcher();
            }
        });
            test('event with handler', function() {
                var eventName = 'simple';

                dispatcher.add(eventName, function() {
                    ok(true, 'received event');
                });
                dispatcher.dispatch(eventName);
            });
            test('event with handler accepting data', function() {
                var eventName = 'simple with data';
                var data = {
                    'bool': true,
                    'int': 1,
                    'string': 'abc',
                    'array': [1, 2, 3],
                    'object': {}
                };

                dispatcher.add(eventName, function(data) {
                    deepEqual(data, data, 'received event');
                });
                dispatcher.dispatch(eventName, data);
            });
            test('events with multiple listener instances', function() {
                expect(3);
                var eventName = 'same events';

                dispatcher.add(eventName, function() {
                    ok(true, 'received in 1st event');
                });
                dispatcher.add(eventName, function() {
                    ok(true, 'received in 2nd event');
                });
                dispatcher.add(eventName, function() {
                    ok(true, 'received in 3rd event');
                });
                dispatcher.dispatch(eventName);
            });
            test('event that does not exist', function() {
                expect(1);
                var eventName = 'does not exist';

                dispatcher.dispatch(eventName);
                ok(true, 'does not call any events');
            });

        module('EventDispatcher.remove()', {
            setup: function() {
                dispatcher = new SNR.events.EventDispatcher();
            }
        });
            test('event with named function handler', function() {
                expect(0);
                var eventName = 'simple';
                var handler = function() {
                    ok(false, 'is not removed');
                };

                dispatcher.add(eventName, handler);
                dispatcher.remove(eventName, handler);
                dispatcher.dispatch(eventName);
            });
            test('event with named function handler accepting data', function() {
                expect(0);
                var eventName = 'simple';
                var data = {
                    'bool': true,
                    'int': 1,
                    'string': 'abc',
                    'array': [1, 2, 3],
                    'object': {}
                };
                var handler = function(data) {
                    ok(false, 'is not removed');
                };

                dispatcher.add(eventName, handler);
                dispatcher.remove(eventName, handler);
                dispatcher.dispatch(eventName);
            });
            test('events with multiple listener instances', function() {
                expect(0);
                var eventName = 'simple';
                var handler = function(data) {
                    ok(false, 'is not removed');
                };

                dispatcher.add(eventName, handler);
                dispatcher.add(eventName, handler);
                dispatcher.add(eventName, handler);
                dispatcher.remove(eventName, handler);
                dispatcher.dispatch(eventName);
            });
            test('event with anonymous function handler', function() {
                expect(0);
                var eventName = 'simple';

                dispatcher.add(eventName, function() {
                    ok(false, 'is not removed');
                });
                dispatcher.remove(eventName);
                dispatcher.dispatch(eventName);
            });
    
    }

    /**
     * Expose API publicly
     */
    return {
        init: init
    };

}(jQuery, document, window, undefined));

/**
 *
 */
jQuery(document).ready(function() {
    main.init();
});