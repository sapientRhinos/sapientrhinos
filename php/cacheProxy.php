<?php

// please note: the cachePath directory must be created before useage
// default is a directory "cache" in the same directory as cacheProxy.php

require('libs/simpleCache.php');
$cache = new SimpleCache();
// defaults
$cachePath = 'cache/';
$cacheSeconds = 600; // 10 minutes

if($_POST['cachePath']){
    $cachePath = $_POST['cachePath'];
}
if($_POST['cacheSeconds']){
    $cacheSeconds = $_POST['cacheSeconds'];
}
$cache->cache_path = $cachePath;
$cache->cache_time = $cacheSeconds;


if($_POST['url']){
    if($_POST['cacheLabel']){
        if($data = $cache->get_cache($_POST['cacheLabel'])){
            $data = $data;
        } else {
            $data = $cache->do_curl($_POST['url']);
            $cache->set_cache($_POST['cacheLabel'], $data);
        }
    }else{
        $data = 'fail, need a url';
    }
}else{
    $data = 'fail, need a url';
}


print_r($data);

?>